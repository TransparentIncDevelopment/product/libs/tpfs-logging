<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Tpfs Logging](#tpfs-logging)
  - [Adding a new crate to the repository](#adding-a-new-crate-to-the-repository)
  - [Faster Check of Source Code without Compiling](#faster-check-of-source-code-without-compiling)
  - [Running Build and Tests](#running-build-and-tests)
  - [Running Linting](#running-linting)
  - [Running Auditing](#running-auditing)
  - [Publishing changes to a crate](#publishing-changes-to-a-crate)
  - [Understanding Semantic Versioning](#understanding-semantic-versioning)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Tpfs Logging

This is a [Ports and Adapters](https://softwarecampament.wordpress.com/portsadapters/)-based
implementation of structured logging for Transparent Systems.

The `tpfs_logger_port` crate represents the `Ports` definition for the logger.

The `tpfs_logger_log4rs_adapter` crate represents the `Adapter` portion implemented for the [`log4rs`](https://github.com/estk/log4rs) crate.

Other crates that are included are for extensions and procmacros.

`tpfs_log_event_procmacro` contains logging event based procmacros and `tpfs_logger_extensions` for extensions.


This interface is designed for maximal ease-of-use by the end-user (the developer doing the logging)
, providing both synchronous and asyncronous logging capability, and a very simple `log()` method
requiring only a severity and an event.  Note the end-user will have to define any events of
interest as `tpfs_logger::Serialize` to interact with this logger library.

## Adding a new crate to the repository

A new folder should be added to this repository with it's own Cargo.toml file and specified at the version of the other crates in this repository.

The folder should also be added to the two following locations:
* [Workspace Cargo.toml](Cargo.toml)
* directories environment variable in [.env](.env) in the order of it's dependency.

The order of dependency means that if the new crate has no dependency to other crates and all other crates depend on it then it would be at the front of the list. On the other hand if it has dependency on all crates in the repository and no crate depends on it then it would be at the end of the list.

## Faster Check of Source Code without Compiling

```shell
cargo check
```

## Running Build and Tests

```shell
cargo make build
cargo make test
```

## Running Linting

```shell
cargo make lint
```

## Running Auditing

```shell
cargo make audit
```

## Publishing changes to a crate

The versioning of the crates are versioned together and should be maintained via [semantic versioning](https://semver.org/) and there are helper scripts to help update the version for those use cases.

Use one of the following:
* `cargo make publish-patch`
* `cargo make publish-minor`
* `cargo make publish-major`

## Understanding Semantic Versioning
The basics of semver is if the only changes included are fixes and adjustments then it's just a patch version number bump. Then if there are additions to functionality it's a minor version bump, and if it's a breaking change then it's a major number bump. The precendence is set at if there's a major version bump for a breaking change it would encompass any additional functionality without needed to bump the minor version. The same is true for minor version bumps that take priority over a patch version bump.

An example of that would be if the version number is `1.0.0` and there was additional functionality along with some fixes this would update the version to `1.1.0`.
